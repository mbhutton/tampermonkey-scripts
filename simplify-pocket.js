// ==UserScript==
// @name         Simplify Pocket
// @namespace    https://bitbucket.org/mbhutton/tampermonkey-scripts
// @version      0.1.0.1
// @description  Remove distractions from Pocket
// @author       Matt Hutton
// @match        https://getpocket.com/a/*
// @grant        none
// @downloadURL  https://bitbucket.org/mbhutton/tampermonkey-scripts/raw/master/simplify-pocket.js
// ==/UserScript==

(function() {
    'use strict';
    $("#pagenav_addarticle").hide(); // '+' button
    $("#pagenav_inbox").hide(); // '+' inbox
    $(".queue_togglesection_text").hide(); // Recommended, and Explore
    $(".pocket_logo").hide();
    $(".side-nav").hide(); // Left side
})();
