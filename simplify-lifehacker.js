// ==UserScript==
// @name         Simplify Lifehacker
// @namespace    https://bitbucket.org/mbhutton/tampermonkey-scripts
// @version      0.1.0.1
// @description  Remove distracting elements from Lifehacker site
// @author       Matt Hutton
// @match        http*://www.lifehacker.com/*
// @match        http*://www.lifehacker.com.au/*
// @grant        none
// @downloadURL  https://bitbucket.org/mbhutton/tampermonkey-scripts/raw/master/simplify-lifehacker.js
// ==/UserScript==

(function() {
    'use strict';
    var to_hide = [
        '.article-comments',
        '.article-trending',
        '.sidebar',
        '.top-stories-container',
        '.site-footer',
    ];
    for (var ind=0; ind < to_hide.length; ind++) {
        $(to_hide[ind]).hide();
    }
})();
