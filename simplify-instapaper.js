// ==UserScript==
// @name         Simplify Instapaper
// @namespace    https://bitbucket.org/mbhutton/tampermonkey-scripts
// @version      0.1.0.1
// @description  Remove distracting elements from Instapaper
// @author       Matt Hutton
// @match        https://www.instapaper.com/*
// @grant        none
// @downloadURL  https://bitbucket.org/mbhutton/tampermonkey-scripts/raw/master/simplify-instapaper.js
// ==/UserScript==

(function() {
    'use strict';
    var to_hide = [
        '.article_add',
        '.article_preview',
        '.author',
        '#liked_nav',
        '#videos_nav',
        '#highlights_nav',
        '#browse_nav',
        '#side_footer',
        '#folder_inline_add_field',
    ];
    for (var ind=0; ind < to_hide.length; ind++) {
        $(to_hide[ind]).hide();
    }
})();
