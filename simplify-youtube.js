// ==UserScript==
// @name         Simplify YouTube
// @namespace    https://bitbucket.org/mbhutton/tampermonkey-scripts
// @version      0.1.0.1
// @description  Remove distracting elements from YouTube
// @author       Matt Hutton
// @match        http*://www.youtube.com/*
// @grant        none
// @run-at       document-end
// @downloadURL  https://bitbucket.org/mbhutton/tampermonkey-scripts/raw/master/simplify-youtube.js
// ==/UserScript==

(function() {
    'use strict';
    function hideByIdOrRetry(id) {
        var e = document.getElementById(id);
        if (e !== null) {
            e.style.visibility = "hidden";
        } else {
            setTimeout(function() { hideByIdOrRetry(id); }, 350);
        }
    }
    hideByIdOrRetry('comments');
    hideByIdOrRetry('related');
})();
