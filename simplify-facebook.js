// ==UserScript==
// @name         Simplify Facebook
// @namespace    https://bitbucket.org/mbhutton/tampermonkey-scripts
// @version      0.1.0.1
// @description  Hide distracting elements from Facebook
// @author       Matt Hutton
// @match        *://*.facebook.com/*
// @grant        none
// @downloadURL  https://bitbucket.org/mbhutton/tampermonkey-scripts/raw/master/simplify-facebook.js
// ==/UserScript==

(function() {
    'use strict';

    /*
    Maintenance notes: There are minor bugs in findAndHideOrRetryWithTimeout, specifically
      around retry behaviour and the feedback that retry produces to the console.
      Nothing which warrants fixing yet.
    */

    var retryMs = 350;
    var timeoutMs = 10000;

    function coerceToArray(obj) {
        if (typeof obj === 'undefined') {
            return [];
        } else if (obj === null) {
            return [];
        } else if (Array.isArray(obj)) {
            return obj;
        } else if (obj.constructor.name === 'HTMLCollection') {
            return Array.from(obj);
        } else {
            return [obj];
        }
    }

    function findAndHideOrRetryWithTimeout(description, findElementsFn, remainingMs) {
        if (remainingMs < 0) {
            console.log("Timed out searching for " + description);
            return;
        }
        var elements = coerceToArray(findElementsFn(document));
        var retry = false;
        if (elements.length > 0) {
            for (var e of elements) {
                if (typeof e.style === 'undefined' || e.style === null) {
                    retry = true;
                } else {
                    e.style.visibility = "hidden";
                }
            }
        } else {
            retry = true;
        }

        if (retry) {
            var retryFn = function() {
                findAndHideOrRetryWithTimeout(description, findElementsFn, remainingMs - retryMs);
            };
            console.log("Will retry later to remove " + description);
            setTimeout(retryFn, retryMs);

        } else {
            console.log("Successfully removed " + elements.length + " elements for " + description);
        }
    }

    function findAndHideOrRetry(description, findElementsFn) {
        findAndHideOrRetryWithTimeout(description, findElementsFn, timeoutMs);
    }

    findAndHideOrRetry("news feed",
        function(d) {
            return d.querySelector('[id^="topnews_"]');
        });

    findAndHideOrRetry("'create' section in bottom left",
        function(d) {
            return d.getElementById('createNav');
        });
    findAndHideOrRetry("shortcuts on left",
        function(d) {
            return d.getElementById('appsNav');
        });
    findAndHideOrRetry("post composer",
        function(d) {
            return d.getElementById('pagelet_composer');
        }
    );
    findAndHideOrRetry("chat bar on right",
        function(d) {
            return d.getElementsByClassName('fbChatSidebar');
        }
    );
    findAndHideOrRetry("right column (1 of 3)",
        function(d) {
            return d.getElementsByClassName('home_right_column');
        }
    );
    findAndHideOrRetry("right column (2 of 3)",
        function(d) {
            return d.getElementById('pagelet_rhc_footer');
        }
    );
    findAndHideOrRetry("right column (3 of 3)",
        function(d) {
            return d.getElementById('pagelet_reminders');
        }
    );
})();
