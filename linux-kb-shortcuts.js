// ==UserScript==
// @name         Linux: mac-like Close,Back,Forward,NewTab
// @namespace    https://bitbucket.org/mbhutton/tampermonkey-scripts
// @version      0.1.0.1
// @description  Cmd+W=Close, Cmd+[=Back, Cmd+]=Forward, Cmd+T=New Tab
// @author       Matt Hutton
// @match        http*://*/*
// @grant        window.close
// @run-at       document-start
// @downloadURL  https://bitbucket.org/mbhutton/tampermonkey-scripts/raw/master/linux-kb-shortcuts.js
// ==/UserScript==

(function() {
    'use strict';

    // Exit if non-Linux 64-bit
    if (typeof window.navigator.platform === 'undefined' ||
        window.navigator.platform === null ||
        window.navigator.platform !== "Linux x86_64") {
        return;
    }

    document.addEventListener ("keydown", function (zEvent) {
        if (!zEvent.ctrlKey && !zEvent.shiftKey && !zEvent.altKey && zEvent.metaKey) { // Cmd only
            if (zEvent.code === "KeyW") { // Cmd+W: Close
                zEvent.stopPropagation();
                zEvent.preventDefault();
                window.close();
            }
            else if (zEvent.code === "BracketLeft") { // Cmd+[: Back in history
                zEvent.stopPropagation();
                zEvent.preventDefault();
                window.history.back();
            }
            else if (zEvent.code === "BracketRight") { // Cmd+]: Forward in history
                zEvent.stopPropagation();
                zEvent.preventDefault();
                window.history.forward();
            }
            else if (zEvent.code === "KeyT") { // Cmd+T: New Tab
                zEvent.stopPropagation();
                zEvent.preventDefault();
                window.open('about:blank', '_blank');
            }
        }
    });
})();
