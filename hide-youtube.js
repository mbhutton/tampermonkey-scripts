// ==UserScript==
// @name         Hide YouTube
// @namespace    https://bitbucket.org/mbhutton/tampermonkey-scripts
// @version      0.1.0.1
// @description  Hides YouTube
// @author       Matt Hutton
// @match        http*://*.youtube.com/*
// @grant        none
// @run-at       document-start
// @downloadURL  https://bitbucket.org/mbhutton/tampermonkey-scripts/raw/master/hide-youtube.js
// ==/UserScript==
// ==/UserScript==

(function() {
    'use strict';
    var imageURL = "https://pics.me.me/assuming-celsius-then-10-ca13-f-look-at-me-its-1241644.png";
    window.location.replace(imageURL);
})();
