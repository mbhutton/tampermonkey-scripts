// ==UserScript==
// @name         Hide Reddit
// @namespace    https://bitbucket.org/mbhutton/tampermonkey-scripts
// @version      0.1.0.2
// @description  Hide Reddit
// @author       Matt Hutton
// @match        http*://*.reddit.com/r/*
// @exclude      http*://*.reddit.com/r/Workflowy/*
// @grant        none
// @downloadURL  https://bitbucket.org/mbhutton/tampermonkey-scripts/raw/master/hide-reddit.js
// ==/UserScript==

(function() {
    'use strict';
    $('.listing-page').hide();
})();
